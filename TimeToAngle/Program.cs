﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeToAngle
{
    class Program
    {
        static void Main(string[] args)
        {
            while(true)
            {
                Console.WriteLine("Enter the hour (0-23)");
                int h = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter minutes (0-59)");
                int m = int.Parse(Console.ReadLine());
                Console.WriteLine($"Time you entered is {h}:{m}");

                if (h >= 12)
                    h -= 12;

                int hAngle = h * 30;
                int mAngle = m * 6;

                int angle = mAngle - hAngle;
                if (angle < 0)
                    angle = 360 + angle;
                Console.WriteLine($"Angle between clock arrows: {angle}");
                Console.WriteLine();
            }
            Console.ReadLine();
        }
    }
}
